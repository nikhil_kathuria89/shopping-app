class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  def index
    @products = Product.all.order("created_at DESC")
  end

  def show
  end

  def new
    @product = Product.new
  end

  def edit
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      redirect_to root_path, notice: 'Product was successfully created.'
    else
       render :new
    end
  end

  def update
    if @product.update(product_params)
       redirect_to root_path, notice: 'Product was successfully updated.'
    else
       render :edit
    end
  end

  def destroy
    @product.destroy
    redirect_to products_url, notice: 'Product was successfully destroyed.'
  end

  def all_products
    @products = Product.paginate(page: params[:page], per_page: 9).order("created_at DESC")
    respond_to do |format|
      format.html
      format.js
    end
  end

  def fetch_product
    @product = Product.find(params[:product_id])
    render :json => @product
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:name, :price, :description, :image)
    end
end
