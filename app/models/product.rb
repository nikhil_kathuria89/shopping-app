class Product < ActiveRecord::Base
  mount_uploader :image, ImageUploader

  validates :name,:price, :description, :presence => true
  validates_numericality_of :price
end
